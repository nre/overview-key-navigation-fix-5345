const {_gi} = imports;
const {Workspace} = imports.ui.workspace;
const {WorkspacesView, WorkspacesViewBase} = imports.ui.workspacesView;


const ST_DIR_TAB_BACKWARD = 1; // From enum StDirectionType.


function init(meta) {
    return new Extension(meta.metadata);
}


class Extension {
    constructor(metadata) {
        this._uuid = metadata.uuid;
        this._patches = [];
    }

    enable() {
        this._patch(
            WorkspacesViewBase, "_init",
            predecessor => function (...args) {
                predecessor.call(this, ...args);
                global.focus_manager.remove_group(this);
            });
        this._patch(
            WorkspacesView, "vfunc_navigate_focus",
            () => function (...args) {
                return this.getActiveWorkspace().navigate_focus(...args, false);
            });
        this._patch(
            Workspace, "_init",
            predecessor => function (...args) {
                predecessor.call(this, ...args);
                if (global.focus_manager.get_group(this) != this) {
                    global.focus_manager.add_group(this);
                }
            });
        this._patch(
            Workspace, "vfunc_navigate_focus",
            predecessor => function (from, direction) {
                // Arrow keys:
                if (direction > ST_DIR_TAB_BACKWARD) {
                    return predecessor.call(this, from, direction);
                }

                // Tab key:
                const focus_chain = this.get_focus_chain();
                if (!focus_chain?.length) {
                    return false;
                }
                if (direction === ST_DIR_TAB_BACKWARD) {
                    focus_chain.reverse();
                }
                const i = (focus_chain.indexOf(from) + 1) % focus_chain.length;
                for (const actor of focus_chain.slice(i)) {
                    if (actor?.navigate_focus?.(from, direction, false)) {
                        return true;
                    }
                }
                return false;
            });
    }

    _patch(...args) {
        const patch = new Patch(...args);
        try {
            patch.enable();
        } catch (e) {
            this._logError(e, `Tried to enable ${patch}`);
            this.disable();
            throw e;
        }
        this._patches.push(patch);
    }

    disable() {
        for (const patch of this._patches) {
            try {
                patch.disable();
            } catch (e) {
                this._logError(e, `Tried to disable ${patch}`);
            }
        }
        this._patches.length = 0;
    }

    _logError(error, message) {
        logError(error, `${this._uuid}: ${message}`);
    }
}


class Patch {
    constructor(class_, methodName, funcFactory) {
        this._class = class_;
        this._proto = class_.prototype;
        this._methodName = methodName;
        this._funcFactory = funcFactory;
        this._predecessor = this._proto[methodName];
    }

    toString() {
        return `[Patch ${this._class?.name}.${this._methodName}]`;
    }

    enable() {
        this._put(this._funcFactory(this._predecessor));
    }

    disable() {
        // Is it standard practice to assume that other extensions haven't
        // monkey-patched the same method? For example:
        // https://gitlab.gnome.org/GNOME/gnome-shell-extensions/-/blob/43.1/extensions/launch-new-instance/extension.js
        this._put(this._predecessor);
    }

    _put(func) {
        const vfuncPrefix = "vfunc_";
        if (this._methodName.startsWith(vfuncPrefix)) {
            const name = this._methodName.slice(vfuncPrefix.length);
            const proto = this._proto[_gi.gobject_prototype_symbol];
            proto[_gi.hook_up_vfunc_symbol](
                name,
                // https://discourse.gnome.org/t/how-to-undo-monkey-patching-a-native-vfunc/12011
                func.toString().includes(" wrapper for native symbol ") ?
                    function (...args) {return func.call(this, ...args)} :
                    func);
        } else {
            this._proto[this._methodName] = func;
        }
    }
}
